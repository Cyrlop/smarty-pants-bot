import argparse
import os
import discord

import pydub
import pydub.playback


class Config:
    discord_bot_token = os.environ.get("DISCORD_BOT_TOKEN_SMARTY")
    sound_alert_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "sounds/horn_01_short.wav"
    )

    main_commands = [
        "smarty:",
        "smarty :",
        "!s ",
    ]


class Client(discord.Client):
    def __init__(self, sound_alert=False, *args, **kw):
        super().__init__(*args, **kw)
        self.config = Config()
        self.config.sound_alert = sound_alert

    async def on_ready(self):
        print(f"Discord client started and logged on as {self.user}!")
        print("--------------------------------")

    async def on_message(self, message):
        # Don't respond to her own messages
        if message.author == self.user:
            return

        # Don't respond if not called by a main_command
        for main_command in self.config.main_commands:
            if message.content.lower().strip().startswith(main_command):
                text = message.content[len(main_command) :].strip()
                break
        else:
            return

        # Play sound alert
        if self.config.sound_alert:
            alert = pydub.AudioSegment.from_wav(self.config.sound_alert_path)
            pydub.playback.play(alert)

        input_message = f"From: {message.author}\n"
        if message.guild:
            input_message += f"Where: {message.guild.name} on #{message.channel}\n"
        else:
            input_message += f"Where: {message.channel}\n"
        input_message += f"Message: {text}\n"
        input_message += f"Your answer:\n"

        response = input(input_message)
        print("--------------------------------")

        if response == "":
            response = "Smarty pants decided not to answer"

        await message.channel.send(response)
        return


def get_intents():
    intents = discord.Intents.default()
    intents.typing = False
    intents.presences = False
    intents.messages = True
    intents.guilds = True
    intents.reactions = True
    return intents


def main(sound_alert=False):
    client = Client(sound_alert=sound_alert, intents=get_intents())
    client.run(client.config.discord_bot_token)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Smarty Pants Bot for discord.")
    parser.add_argument(
        "--sound-alert",
        "-s",
        action="store_true",
        help="If True, plays a sound alert when you receive a prompt to answer.",
    )
    args = parser.parse_args()
    main(**vars(args))
