# Smarty Pants Bots
A fake discord bot that let's you type the answers and pretend the bot just did.

## Usage
```
usage: main.py [-h] [--sound-alert]

Smarty Pants Bot for discord.

optional arguments:
  -h, --help         show this help message and exit
  --sound-alert, -s  if True, plays a sound alert when you receive a prompt
                     to answer (requires simpleaudio, pyaudio or ffmpeg)
```